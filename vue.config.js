module.exports = {
  publicPath:
    process.env.NODE_ENV === 'production'
      ? '/~cs62160295/learn_bootstrap/'
      : '/'
}
